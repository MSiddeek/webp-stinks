package com.msiddeek.web2png;

import java.lang.reflect.Field;
import java.util.Arrays;

public class JavaLibraryPath {

    public static void append(String pathToAdd) throws Exception{
        System.out.println("java.library.path append...");
        final Field usrPathsField = ClassLoader.class.getDeclaredField("usr_paths");
        usrPathsField.setAccessible(true);

        //get array of paths
        final String[] paths = (String[])usrPathsField.get(null);

        //check if the path to add is already present
        for(int i = 0; i < paths.length; ++i) {
            if(paths[i].equals(pathToAdd)) {
                return;
            }
        }

        //add the new path
        final String[] newPaths = new String[paths.length + 1];
        for (int i = 0; i < paths.length; ++i) {
            newPaths[i] = paths[i];
        }
        newPaths[newPaths.length - 1] = pathToAdd;

        usrPathsField.set(null, newPaths);
        System.out.println("java.library.path=");
        for (int i = 0; i < newPaths.length; ++i) {
            System.out.println("    " + newPaths[i]);
        }
    }
}
