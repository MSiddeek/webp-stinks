package com.msiddeek.web2png;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Zip {

	public static void unzip(File zipFile, File outputFolder) throws IOException {
		System.out.println("unzip...");
		if (!outputFolder.exists()) {
			outputFolder.mkdirs();
		}

		byte[] buffer = new byte[1024];
		ZipArchiveInputStream zis = new ZipArchiveInputStream(new FileInputStream(zipFile));
		ArchiveEntry ze = zis.getNextEntry();
		System.out.println(ze);

		while (ze != null) {
			String fileName = ze.getName();
			File newFile = new File(outputFolder, fileName);
			newFile.getParentFile().mkdirs();

			if (ze.isDirectory()) {
				newFile.mkdirs();
			} else {
				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
			}
			ze = zis.getNextEntry();
			System.out.println(ze);
		}
		zis.close();

		System.out.println(outputFolder + " unziped.");
	}
}