package com.msiddeek.webpstinks.utils;

import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.vfs.VfsUtilCore;
import com.intellij.openapi.vfs.VirtualFile;
import com.msiddeek.webpstinks.files.LookForDrawableFilesVisitor;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AndroidDirUtils {
    private static final Logger LOG = Logger.getInstance(AndroidDirUtils.class);

    private static String getResFileContent(final Editor editor) {
        if (editor != null && editor.getDocument() != null) {
            return editor.getDocument().getText();
        } else {
            return "";
        }
    }

    private static String getResFileContent(VirtualFile file) {
        Document document = FileDocumentManager.getInstance().getDocument(file);
        return document == null ? "" : document.getText();
    }

    private static Set<String> extractDrawableNames(final String fileContent) {
        Set<String> webps = new HashSet<String>();
        final Pattern p = Pattern.compile("\"@drawable/.+\"");
        final Matcher m = p.matcher(fileContent);
        while (m.find()) {
            final String reference = m.group();
            webps.add(reference.substring(11, reference.length() - 1));
        }
        return webps;
    }

    private static List<String> extractDrawableNames(Editor[] editors) {
        LOG.info("Extracting webp names...");
        final List<String> webps = new ArrayList<String>();
        for (Editor editor : editors) {
            webps.addAll(extractDrawableNames(getResFileContent(editor)));
        }
        return webps;
    }


    private static List<String> extractDrawableNames(Set<VirtualFile> xmls) {
        final List<String> webps = new ArrayList<String>();
        for (VirtualFile xml : xmls) {
            webps.addAll(extractDrawableNames(getResFileContent(xml)));
        }
        return webps;
    }

    public static Set<VirtualFile> getWebpsInDocument(VirtualFile root, int maxIterations) {
        Set<VirtualFile> webps = new HashSet<VirtualFile>();

        List<String> drawables = extractDrawableNames(EditorFactory.getInstance().getAllEditors());
        LookForDrawableFilesVisitor lookForWebpVisitor = new LookForDrawableFilesVisitor(drawables);

        LOG.info("Traversing tree(0) " + root + " drawables " + drawables + "...");
        VfsUtilCore.visitChildrenRecursively(root, lookForWebpVisitor);
        webps.addAll(lookForWebpVisitor.getWebps());

        for (int i = 1; i <= maxIterations && !lookForWebpVisitor.getXmls().isEmpty(); ++i) {
            drawables = extractDrawableNames(lookForWebpVisitor.getXmls());
            lookForWebpVisitor = new LookForDrawableFilesVisitor(drawables);

            LOG.info("Traversing tree(" + i + ") " + root + " drawables " + drawables + "...");
            VfsUtilCore.visitChildrenRecursively(root, lookForWebpVisitor);
            webps.addAll(lookForWebpVisitor.getWebps());
        }
        return webps;
    }

}
