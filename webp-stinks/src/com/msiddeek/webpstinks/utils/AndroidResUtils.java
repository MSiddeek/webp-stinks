package com.msiddeek.webpstinks.utils;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.util.Computable;
import com.intellij.openapi.vfs.VirtualFile;
import com.msiddeek.web2png.WebP;
import org.eclipse.jdt.internal.compiler.ProcessTaskManager;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class AndroidResUtils {
    private static final Logger LOG = Logger.getInstance(AndroidResUtils.class);

    public static Set<VirtualFile> convert(Set<VirtualFile> webpFiles, String outputFormat, int maxRetries) {
        final Set<VirtualFile> result = new HashSet<VirtualFile>();
        for (VirtualFile webpFile : webpFiles) {
            result.add(ApplicationManager.getApplication()
                    .runWriteAction(new ConvertRunnable(webpFile, outputFormat, maxRetries)));
        }
        return result;
    }

    public static void delete(Set<VirtualFile> files, int maxRetries, int cleanupDelay) {
        Timer timer = new Timer(cleanupDelay, new DeleteLaterRunnable(files, maxRetries));
        timer.setRepeats(false);
        timer.setInitialDelay(cleanupDelay);
        timer.start();
    }

    private static class ConvertRunnable implements Computable<VirtualFile> {

        private final VirtualFile mInput;
        private final String mTargetFormat;
        private final int mMaxRetries;

        private static VirtualFile convertWebP(VirtualFile in, String targetFormat) throws IOException {
            BufferedImage inputImage = WebP.read(in.getInputStream());
            VirtualFile outputFile = makeFile(in, targetFormat);

            ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream();
            ImageIO.write(inputImage, targetFormat, outputBuffer);
            outputFile.setBinaryContent(outputBuffer.toByteArray());
            return outputFile;
        }

        private static VirtualFile makeFile(VirtualFile webp, String newExtension) throws IOException {
            final String newName = webp.getNameWithoutExtension() + "." + newExtension;
            final VirtualFile newFile = webp.getParent().findOrCreateChildData(null, newName);
            if (newFile.exists())
                newFile.delete(null);
            return webp.getParent().createChildData(null, newName);
        }

        private ConvertRunnable(VirtualFile inputFile, String outputFormat, int maxRetries) {
            mInput = inputFile;
            mTargetFormat = outputFormat;
            mMaxRetries = maxRetries;
        }

        @Override
        public VirtualFile compute() {
            for (int retry = 0; retry < mMaxRetries; ++retry) {
                try {
                    LOG.info("Converting " + mInput + "...");
                    return convertWebP(mInput, mTargetFormat);
                } catch (IOException e) {
                    LOG.error(e);
                }
            }
            return null;
        }
    }

    private static class DeleteLaterRunnable implements ActionListener {
        private final Set<VirtualFile> mFiles;
        private final int mMaxRetries;

        private DeleteLaterRunnable(Set<VirtualFile> files, int maxRetries) {
            mFiles = files;
            mMaxRetries = maxRetries;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ApplicationManager.getApplication()
                    .runWriteAction(new WaitAndDeleteComputable(mFiles, mMaxRetries));
        }
    }

    private static class WaitAndDeleteComputable implements Runnable {
        private final Set<VirtualFile> mFiles;
        private final int mMaxRetries;

        private WaitAndDeleteComputable(Set<VirtualFile> files, int maxRetries) {
            mFiles = files;
            mMaxRetries = maxRetries;
        }

        @Override
        public void run() {
            for (VirtualFile in : mFiles) {
                for (int retry = 0; retry < mMaxRetries; ++retry) {
                    try {
                        if (in.exists()) {
                            LOG.info("Deleting " + in + "...");
                            in.delete(in);
                        }
                        break;
                    } catch (IOException e) {
                        LOG.error(e);
                    }
                }
            }
            mFiles.clear();
        }
    }
}
