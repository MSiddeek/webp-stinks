If you use Android Studio always wished `WebP` images could show in the designer preview, then this is the plugin for you.  Because Web Pee stinks!

### How to install? ###

* Download a build from the [download page](https://bitbucket.org/MSiddeek/webp-stinks/downloads).
* Follow the steps [here](https://www.jetbrains.com/help/idea/2016.1/installing-plugin-from-disk.html?origin=old_help).

### How to develop? ###
```
TODO
```

### How does it work? ###

When you hit the refresh button on the preview panel:

1. The plugin looks for `@drawable` references (including the transitive ones) given the file you have open.
2. Out of those, it converts all the `WebP` drawables into `PNG`s and puts them in the same directory.
3. It lets the preview in Android studio refresh.  The preview picks the `PNG`s and displays them.
4. WebpStinks cleans up the `PNG`s it generated after a couple of seconds.

### What to improve? ###

* Include conversion library properly;  Currently we download the library, extract it, then add it to the `java.library.path` during runtime.
* Make the refresh automatic.

## Update ##
Seems [a fix](https://code.google.com/p/android/issues/detail?id=47622#makechanges) is already in the works.

![https://memeexplorer.com/cache/454.jpg](https://memeexplorer.com/cache/454.jpg)

### Screenshot ###
![https://plugins.jetbrains.com/files/8337/screenshot_15843.png](https://plugins.jetbrains.com/files/8337/screenshot_15843.png)
